function PenelopeStackClient(stack) {
  if (!stack) throw new Error('stack is required');
  
  return function (socket, params) {
    if (!socket) throw new Error('socket is required');
    
    socket
    .on('describe-apis', function (callback) {
      callback(null, stack.listPlugins());
    });
    
    socket
    .on('api', function (token, pluginName, methodName, methodParams, callback) {
      stack
      .simpleInvokeMethod(token, pluginName, methodName, methodParams)
      .then(function (result) {
        callback(null, result);
      })
      .catch(function (err) {
        callback(err);
      });
    });
    
    return Promise.resolve();
  };
}

module.exports = PenelopeStackClient;