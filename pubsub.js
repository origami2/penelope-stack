function PenelopeStackPubSub(stack) {
  if (!stack) throw new Error('stack is required');
  
  return function (socket, params) {
    if (!socket) throw new Error('socket is required');
    
    socket
    .on('subscribe', function (namespace) {
      stack.addSubscriber(socket, namespace);
    });
    
    socket
    .on('unsubscribe', function (namespace) {
      stack.removeSubscriber(socket, namespace);
    });
    
    socket
    .on('publish', function (namespace, token, message) {
      stack
      .publishEvent(
        namespace,
        token,
        message,
        function (filteringSocket) {
          return filteringSocket !== socket;
        }
      );
    });
    
    return Promise.resolve();
  };
}

module.exports = PenelopeStackPubSub;