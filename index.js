
var Plugin = require('./plugin');
var Client = require('./client');
var PubSub = require('./pubsub');

function PenelopeStack(stack) {
  if (!stack) throw new Error('stack is required');
  
  return function (socket, params) {
    if (!socket) throw new Error('socket is required');
    if (!params) throw new Error('params are required');
    if (!params.namespace) throw new Error('namespace is required');
    
    if (params.namespace.indexOf('Plugin.') === 0) {
      (new Plugin(stack))(socket, params);
    } else {
      (new Client(stack))(socket, params);
    }
  };
}

PenelopeStack.Plugin = Plugin;
PenelopeStack.Client = Client;
PenelopeStack.PubSub = PubSub;

module.exports = PenelopeStack;