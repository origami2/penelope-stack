var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('Penelope Stack', function () {
  describe('Plugin', function () {
    var PenelopeStackPlugin;
    
    beforeEach(function () {
      PenelopeStackPlugin = require('..').Plugin;
    });
    
    it('returns a function', function () {
      assert.equal('function', typeof(PenelopeStackPlugin));
    });
    
    it('requires an stack', function () {
      assert.throws(
        function () {
          new PenelopeStackPlugin();
        },
        /stack is required/
      );
    });
    
    it('accepts stack', function () {
      var stack = {};
      new PenelopeStackPlugin(stack);
    });
    
    it('returns a function', function () {
      var stack = {};
      var target = new PenelopeStackPlugin(stack);
      
      assert.equal('function', typeof(target));
    });
    
    it('requires socket', function () {
      var stack = {};
      var target = new PenelopeStackPlugin(stack);
      
      assert.throws(
        function () {
          target(null, {});
        },
        /socket is required/
      );
    });
    
    it('requires socket', function () {
      var stack = {};
      var target = new PenelopeStackPlugin(stack);
      
      assert.throws(
        function () {
          target(new EventEmitter(), null);
        },
        /params are required/
      );
    });
    
    it('requires name in params', function () {
      var stack = {};
      var target = new PenelopeStackPlugin(stack);
      var socket = new EventEmitter();
      
      assert.throws(
        function () {
          target(socket, { methods: {} });
        },
        /name is required/
      );
    });
    
    it('requires methods in params', function () {
      var stack = {};
      var target = new PenelopeStackPlugin(stack);
      var socket = new EventEmitter();
      
      assert.throws(
        function () {
          target(socket, { name: 'Plugin1' });
        },
        /methods are required/
      );
    });
    
    it('invokes stack.addPlugin', function (done) {
      var fakeSocket = new EventEmitter();
      var stack = {
        addPlugin: function (pluginName, socket, methods) {
          try {
            assert.equal('Plugin1', pluginName);
            assert.equal(fakeSocket,socket);
            assert.deepEqual({ method1: [ 'param1' ] }, methods);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      };
      var target = new PenelopeStackPlugin(stack);
      
      target(fakeSocket, { name: 'Plugin1', methods: { method1: ['param1'] } });
    });
  });
});