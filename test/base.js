var PenelopeStack = require('..');
var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('Penelope Stack', function () {
  it('returns a function', function () {
    assert.equal('function', typeof(PenelopeStack));
  });
  
  it('requires stack', function () {
    assert.throws(
      function () {
        new PenelopeStack();
      },
      /stack is required/
    );
  });
  
  it('creates a function', function () {
    var target = new PenelopeStack({});
    
    assert.equal('function', typeof(target));
  });
  
  it('requires socket', function () {
    var target = new PenelopeStack({});
    
    assert.throws(
      function () {
        target();
      },
      /socket is required/
    );
  });
  
  it('requires params', function () {
    var target = new PenelopeStack({});
    
    assert.throws(
      function () {
        target(new EventEmitter());
      },
      /params are required/
    );
  });
  
  it('requires namespace', function () {
    var target = new PenelopeStack({});
    
    assert.throws(
      function () {
        target(new EventEmitter(), {});
      },
      /namespace is required/
    );
  });
  
  it('handles namespace begining with Plugin.', function () {
    var target = new PenelopeStack({
      addPlugin: function () {}
    });
    
    target(
      new EventEmitter(), 
      {
        name: 'Plugin1',
        methods: {},
        namespace: 'Plugin.StubPlugin1'
      }
    );
  });
});