var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('Penelope Stack', function () {
  describe('Client', function () {
    var PenelopeStackClient;
    
    beforeEach(function () {
      PenelopeStackClient = require('..').Client;
    });
    
    it('returns a function', function () {
      assert.equal('function', typeof(PenelopeStackClient));
    });
    
    it('requires a socket', function () {
      var stack = {};
      var target = new PenelopeStackClient(stack);
 
      assert.throws(
        function () {
          target();
        },
        /socket is required/
      );
    });
    
    it('listens describe-api and invokes listPlugins', function (done) {
      var stack = {
        listPlugins: function () {
          done();
          
          return {};
        }
      };
      var target = new PenelopeStackClient(stack);
      
      var socket = new EventEmitter();
 
      target(socket);
      
      socket
      .emit(
        'describe-apis',
        function () {
        }
      );
    });
    
    it('listens api and invokes invokeMethod', function (done) {
      var stack = {
        simpleInvokeMethod: function (token, pluginName, methodName, methodParams) {
          try {
            assert.deepEqual({ theToken: 'abcd124'}, token);
            assert.equal('Plugin1', pluginName);
            assert.equal('method1', methodName);
            assert.deepEqual({ param1: 'param1' }, methodParams);
            
            done();
          } catch (e) {
            done(e);
          }
          
          return Promise.resolve();
        }
      };
      
      var target = new PenelopeStackClient(stack);
      var socket = new EventEmitter();
 
      target(socket);
      
      socket
      .emit(
        'api',
        { theToken: 'abcd124'},
        'Plugin1',
        'method1',
        { param1: 'param1' },
        function () {
        }
      );
    });
    
    it('invokes api callback with error', function (done) {
      var stack = {
        simpleInvokeMethod: function (token, pluginName, methodName, methodParams) {
          return Promise.reject('my error');
        }
      };
      
      var target = new PenelopeStackClient(stack);
      var socket = new EventEmitter();
 
      target(socket);
      
      socket
      .emit(
        'api',
        { theToken: 'abcd124'},
        'Plugin1',
        'method1',
        { param1: 'param1' },
        function (err) {
          try {
            assert.equal('my error', err);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });

    it('invokes api callback with result', function (done) {
      var stack = {
        simpleInvokeMethod: function (token, pluginName, methodName, methodParams) {
          return Promise.resolve('my result');
        }
      };
      
      var target = new PenelopeStackClient(stack);
      var socket = new EventEmitter();
 
      target(socket);
      
      socket
      .emit(
        'api',
        { theToken: 'abcd124'},
        'Plugin1',
        'method1',
        { param1: 'param1' },
        function (err, result) {
          try {
            assert(!err);
            assert.equal('my result', result);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  });
});