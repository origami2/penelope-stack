var assert = require('assert');
var EventEmitter = require('events').EventEmitter;

describe('Penelope Stack', function () {
  describe('PubSub', function () {
    var PenelopeStackPubSub;
    
    beforeEach(function () {
      PenelopeStackPubSub = require('..').PubSub;
    });
    
    it('returns a function', function () {
      assert.equal('function', typeof(PenelopeStackPubSub));
    });
    
    it('requires a socket', function () {
      var stack = {};
      var target = new PenelopeStackPubSub(stack);
 
      assert.throws(
        function () {
          target();
        },
        /socket is required/
      );
    });
    
    it('invokes addSubscriber', function (done) {
      var fakeSocket = new EventEmitter();
      var stack = {
        addSubscriber: function (socket, namespace) {
          try {
            assert.equal(fakeSocket, socket);
            assert.equal('MyNamespace', namespace);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      };
      
      var target = new PenelopeStackPubSub(stack);
      target(fakeSocket);
 
      fakeSocket.emit('subscribe', 'MyNamespace');
    });
    
    it('invokes removeSubscriber', function (done) {
      var fakeSocket = new EventEmitter();
      var stack = {
        addSubscriber: function (socket, namespace) {
        },
        removeSubscriber: function (socket, namespace) {
          try {
            assert.equal(fakeSocket, socket);
            assert.equal('MyNamespace', namespace);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      };
      
      var target = new PenelopeStackPubSub(stack);
      target(fakeSocket);
 
      fakeSocket.emit('subscribe', 'MyNamespace');
      fakeSocket.emit('unsubscribe', 'MyNamespace');
    });
    
    it('invokes publishEvent', function (done) {
      var fakeSocket = new EventEmitter();
      var stack = {
        publishEvent: function (namespace, token, message, socketFilter) {
          try {
            assert.equal('MyNamespace', namespace);
            assert.deepEqual({ theToken: true }, token);
            assert.deepEqual({ theMessage: true }, message);
            assert.equal('function', typeof(socketFilter));
            assert(!socketFilter(fakeSocket));
            assert(socketFilter(new EventEmitter()));
            
            done();
          } catch (e) {
            done(e);
          }
        }
      };
      
      var target = new PenelopeStackPubSub(stack);
      target(fakeSocket);
 
      fakeSocket.emit('publish', 'MyNamespace', { theToken: true }, { theMessage: true });
    });
  });
});