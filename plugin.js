function PenelopeStackPlugin(stack) {
  if (!stack) throw new Error('stack is required');
  
  return function (socket, params) {
    if (!socket) throw new Error('socket is required');
    if (!params) throw new Error('params are required');
    if (!params.name) throw new Error('name is required');
    if (!params.methods) throw new Error('methods are required');
    
    stack
    .addPlugin(
      params.name, 
      socket, 
      params.methods
    );
    
    return Promise.resolve();
  };
}

module.exports = PenelopeStackPlugin;